const https = require('https');
const cors = require('cors');
const express = require('express');
const app = express();

app.use(cors());
app.get('/api/audit/',  (req, res) => {
    getAudit(req.query.url, res);
});

const port = 3000;
app.listen(port, ()=>{ console.log(`Listening on ${port}...`) });

function getAudit(url, response) {
    https.get(`https://www.googleapis.com/pagespeedonline/v5/runPagespeed/?url=${url}`, (res) => {
        let data = '';
        res.on('data', (chunk) => {
            data += chunk;
        })
        res.on('end', () => { 
            data = JSON.parse(data);
            if (data.lighthouseResult && data.lighthouseResult.audits){
                response.send(JSON.stringify(data.lighthouseResult.audits));
            } else {
                response.status(400).send('Not found');
            }
        })
    });
}
