Angular CLI, NPM  must be installed.

1. Install api dependencies: 'cd nodejs_api' and run 'npm install'

2. Install front-end dependencies: 'cd audit-app' and run 'npm install'

3. Start application: 'cd nodejs_api' and 'node server.js',  'cd audit-app' and 'ng serve -o'